﻿using PeliculasSeries.Utilidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PeliculasSeries.Controllers
{
    public class ProductorasController : Controller
    {
        // GET: Productoras
        public ActionResult Index()
        {
            Utiles.EstablecerConexion();
            var productoras = from e in Utiles.db.Productoras orderby e.Clave select e;
            return View(productoras);
        }
    }
}