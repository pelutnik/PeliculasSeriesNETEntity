﻿using PeliculasSeries.Utilidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PeliculasSeries.Controllers
{
    public class PeliculasController : Controller
    {
        // GET: Peliculas
        public ActionResult Index()
        {
            Utiles.EstablecerConexion();
            var peliculas = from pel in Utiles.db.Peliculas
                 //           join format in Utiles.db.Formatos on format.
                            orderby pel.Clave select pel;
            return View(peliculas);

        }

        //GET: Peliculas/Create
        public ActionResult Create()
        {
            return View();
        }

    }
}