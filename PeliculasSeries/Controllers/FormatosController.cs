﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PeliculasSeries.Models;
using PeliculasSeries.Utilidades;

namespace PeliculasSeries.Controllers
{
    public class FormatosController : Controller
    {
        

        // GET: Formatos
        public ActionResult Index()
        {
            Utiles.EstablecerConexion();
            var formatos = from e in Utiles.db.Formatos orderby e.Id select e;
            return View(formatos);
            //formatos = null;
        }

        // GET: Formatos/Details/5
        public ActionResult Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Utiles.EstablecerConexion();
            Formato formato = Utiles.db.Formatos.Find(id);
            if (formato == null)
            {
                return HttpNotFound();
            }
            return View(formato);
        }

        // GET: Formatos/Create
       /* public ActionResult Create()
        {
            return View();
        }*/

        // POST: Formatos/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
       /* [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Clave,Nombre,Descripcion")] Formato formato)
        {
            if (ModelState.IsValid)
            {
                db.Formatos.Add(formato);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(formato);
        }*/

        // GET: Formatos/Edit/5
        public ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Utiles.EstablecerConexion();
            Formato formato = Utiles.db.Formatos.Find(id);
            if (formato == null)
            {
                return HttpNotFound();
            }
            return View(formato);
        }

        // POST: Formatos/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Clave,Nombre,Descripcion")] Formato formato)
        {
            if (ModelState.IsValid)
            {
                Utiles.EstablecerConexion();
                Utiles.db.Entry(formato).State = EntityState.Modified;
                Utiles.db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(formato);
        }
        /*
        // GET: Formatos/Delete/5
        public ActionResult Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Formato formato = db.Formatos.Find(id);
            if (formato == null)
            {
                return HttpNotFound();
            }
            return View(formato);
        }

        // POST: Formatos/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(long id)
        {
            Formato formato = db.Formatos.Find(id);
            db.Formatos.Remove(formato);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }*/
    }
}
