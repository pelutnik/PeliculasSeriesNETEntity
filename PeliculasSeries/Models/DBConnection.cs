﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace PeliculasSeries.Models
{
    public class DBConnection : DbContext
    {
        public DBConnection() : base("name=PeliculasSeries")
        {

        }

        public DbSet<Pelicula> Peliculas { get; set; }
        public DbSet<Formato> Formatos { get; set; }
        public DbSet<Productora> Productoras { get; set; }
    }
}