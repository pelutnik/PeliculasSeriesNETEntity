﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PeliculasSeries.Models
{
    public class Pelicula
    {
        public long Id { get; set; }
        public string Clave { get; set; }
        public string Nombre { get; set; }
        public string Formato { get; set; }
        public string Tipo { get; set; }
        public string Productoras { get; set; }
        public int Temporada { get; set; }
        public string Sipnosis { get; set; }
        public string Estados { get; set; }
        public string Generos { get; set; }
    }
}