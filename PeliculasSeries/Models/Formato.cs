﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PeliculasSeries.Models
{
    public class Formato
    {
        public long Id { get; set; }
        public string Clave { get; set; }
        public string Nombre { get; set; }
        public string Descripcion { get; set; }
    }
}