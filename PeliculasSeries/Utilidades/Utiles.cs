﻿using PeliculasSeries.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PeliculasSeries.Utilidades
{
    public static class Utiles
    {
        public static DBConnection db;

        public static void EstablecerConexion()
        {
            db = new DBConnection();
        }
    }
}